package Repository;

public class RequestBody extends Environment {

	public static String req_tc1() {
		String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		return req_body;
	}

	public static String req_tc2() {
		String req_body = "{\r\n" + "    \"name\": \"SWAPNEEL\",\r\n" + "    \"job\": \"QA\"\r\n" + "}";
		return req_body;
	}
}