package Repository;

public class Environment {

	public static String Hostname() {
		String Hostname = "https://reqres.in/";
		return Hostname;
	}

	public static String Resource() {
		String Resource = "api/users/2";
		return Resource;
	}

	public static String HeaderName() {

		String HeaderName = "Content-Type";
		return HeaderName;
	}

	public static String HeaderValue() {

		String HeaderValue = "application/json";
		return HeaderValue;
	}
}