package Request_Specification_Class;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Request_Post_Api {

	public static void main(String[] args) {
	
		// Step 1 : Collect all needed information and save it into local variables

				String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

				String hostname = "https://reqres.in/";

				String resource = "api/users";

				String headername = "Content-Type";

				String headervalue = "application/json";
				
				// Step 2 : Build the request specification using RequestSpecification class

				RequestSpecification requestSpec = RestAssured.given();

				// Step 2.1 : Set request header

				requestSpec.header(headername, headervalue);

				// Step 2.2 : Set request body

				requestSpec.body(req_body);

				// Step 3 : Send the API request

				Response response = requestSpec.post(hostname + resource);

				// Step 4 : Parse the response body

				ResponseBody res_body = response.getBody();

				String res_name = res_body.jsonPath().getString("name");
				System.out.println(res_name);
				String res_job = res_body.jsonPath().getString("job");
				System.out.println(res_job);
				String res_id = res_body.jsonPath().getString("id");
				System.out.println(res_id);
				String res_createdAt = res_body.jsonPath().getString("createdAt");
				res_createdAt = res_createdAt.substring(0, 11);
				System.out.println(res_createdAt);

				// Step 5 : Validate the response body

				// Step 5.1 : Parse request body and save into local variables

				JsonPath jsp_req = new JsonPath(req_body);
				String req_name = jsp_req.getString("name");
				String req_job = jsp_req.getString("job");

				// Step 5.2 : Generate expected date

				LocalDateTime currentdate = LocalDateTime.now();
				String expecteddate = currentdate.toString().substring(0, 11);

				// Step 5.3 : Use TestNG's Assert

				Assert.assertEquals(res_name, req_name);
				Assert.assertEquals(res_job, req_job);
				Assert.assertNotNull(res_id);
				Assert.assertEquals(res_createdAt, expecteddate);

			}

		}


	
