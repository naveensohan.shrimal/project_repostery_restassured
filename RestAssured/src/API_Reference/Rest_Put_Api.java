package API_Reference;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;
public class Rest_Put_Api {

	public static void main(String[] args) {
		// Step 1 : Collect all needed information and save it into local variables

		String req_body = "{\n"
				+ "    \"name\": \"morpheus\",\n"
				+ "    \"job\": \"zion resident\"\n"
				+ "}";
		String hostname = "https://reqres.in";
		String resource = "/api/users/2";
		String headername = "Content-Type";
		String headervalue = "application/json";
		
		
		//declare base URI
		
		RestAssured.baseURI = hostname;
		
		
		//configure api
		
		String res_body = given().header(headername , headervalue).body(req_body)
		.when().put(resource)
		.then().extract().response().asString();
		
		System.out.println(res_body);
		
		
		// configure api with status code
		
		int status_code = given().header(headername , headervalue).body(req_body)
				.when().put(resource)
				.then().extract().statusCode();
		
		System.out.println(status_code);

		//parse JSon path for Response Body
		
		JsonPath j_res = new JsonPath(res_body);
		
		String res_name = j_res.getString("name");
		System.out.println(res_name);
		String res_job = j_res.getString("job");
		System.out.println(res_job);
		String res_updatedAt = j_res.getString("updatedAt");
		System.out.println(res_updatedAt);
		
		res_updatedAt = res_updatedAt.substring(0,11);
		
		//System.out.println(res_updatedAt);

		
		LocalDateTime currentdate = LocalDateTime.now();
		String  Expectedate = currentdate.toString().substring(0, 11);
		
		//System.out.println(Expectedate);
		
		
		//parse JSon path for Request Body
		
        JsonPath j_req = new JsonPath(req_body);
		
		String req_name = j_req.getString("name");
		//System.out.println(req_name);
		String req_job = j_req.getString("job");
		//System.out.println(req_job);


		//use TestNG
		
		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(req_job, res_job);
		Assert.assertEquals(res_updatedAt, Expectedate);

}

}
