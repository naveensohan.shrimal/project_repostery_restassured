package API_Reference;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;

import org.testng.Assert;
public class Rest_Post_Api_Sucessful_200 {

	public static void main(String[] args) {
		String req_body = "{\n"
				+ "    \"email\": \"eve.holt@reqres.in\",\n"
				+ "    \"password\": \"cityslicka\"\n"
				+ "}";
		String hostname = "https://reqres.in";
		String resource = "/api/login";
		String headername = "Content-Type";
		String headervalue = "application/json";
		
	  //declare base URI
		
	   RestAssured.baseURI = hostname;
		
	  // configure API
	   
	   String res_body = given().header(headername , headervalue).body(req_body)
	   .when().post(resource)
	   .then().extract().response().asString();
	   
	   System.out.println(res_body);
	   
	   int status_code = given().header(headername , headervalue).body(req_body)
			   .when().post(resource)
			   .then().extract().statusCode();
			   
			   System.out.println(status_code);
	   
	  //parse json path
			   
	   JsonPath j_res = new JsonPath(res_body);
	   String res_token = j_res.getString("token");
	   
	   //System.out.println(res_token);

	   
	  //use testNG	
	   
	   Assert.assertNotNull(res_token);

	}

}


