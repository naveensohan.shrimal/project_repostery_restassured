package Common_Methods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utility {

	public static void readExcelData(String SheetName, String TestCase) throws IOException {
		// Step 1 : Fetch the Java project name and location

		String projectDir = System.getProperty("user.dir");
		System.out.println("Current project Directory Is :" + projectDir);

		// Step 2 : Create an object of file input stream to locate the excel file

		FileInputStream fls = new FileInputStream(projectDir + "\\DataFiles\\Input_Data.xlsx");

		// step 3: create an object to XSSFWorkbook to open the excel file

		// Step 3.1: Fetch the count sheets
		XSSFWorkbook wb = new XSSFWorkbook(fls);
		int count = wb.getNumberOfSheets();
		System.out.println("Count of sheets is :" + count);

		// step 4: fetch the names of sheets:

		for (int i = 0; i < count; i++) {

			if (wb.getSheetName(i).equals(SheetName)) {
				System.out.println("sheet at index " + i + ":" + wb.getSheetName(i));
				break;
			} else {
				System.out.println(SheetName + "sheet not found in file Input_Data.xlsx at index:" + i);
			}
		}
	}

	public static void evidenceFileCreator(String Filename, File FileLocation, String RequestBody, String ResponseBody,
			String ResHeader, String Endpoint) throws IOException

	{
		// Step 1 : Create and open the file
		File newTextFile = new File(FileLocation + "\\" + Filename + ".txt");
		System.out.println("File created with name:" + newTextFile.getName());

		// Step 2 : Write the data

		FileWriter writedata = new FileWriter(newTextFile);
		writedata.write("Endpoint is :\n" + Endpoint + "\n\n");
		writedata.write("Request body is :\n" + RequestBody + "\n\n");
		writedata.write("Response header date is : \n" + ResHeader + "\n\n");
		writedata.write("Response body is : \n" + ResponseBody);

		// STep 3 : save and close

		writedata.close();
	}

	public static File CreateLogDirectory(String dirName) {

		// Step 1 : Fetch the java project and location

		String projectDir = System.getProperty("user.dir");
		System.out.println("Current Project Directory is :" + projectDir);

		// Step 2 : Verify whether the directory in variable dirName exists inside
		// projectDir and act accordingly

		File directory = new File(projectDir + "\\" + dirName);

		if (directory.exists()) {
			System.out.println(directory + ", Already Exists");
		} else {

			System.out.println(directory + ", Dosen't Exists , hence Creating it");
			directory.mkdir();
			System.out.println((directory + ", created"));

		}

		return directory;

	}

	public static String testLogName(String Name) {
		LocalTime currentTime = LocalTime.now();
		String currentstringTime = currentTime.toString().replaceAll(":", "");
		String TestLogName = "Test_Case_1" + currentstringTime;
		return TestLogName;

	}
}